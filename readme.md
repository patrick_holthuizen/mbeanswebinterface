Installation instructions
=========================

These instructions only apply for Oracle WebLogic Server 12c.

1. Export a WAR file from the project.
2. Install the WAR file as a regular deployment.